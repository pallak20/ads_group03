<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Health Insurance Marketplace</title>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>
			body {
                background-image: url(http://previews.123rf.com/images/wawritto/wawritto1303/wawritto130300065/18539594-Abstract-molecules-medical-background-Vector--Stock-Vector.jpg);
				background-repeat: no-repeat;
				background-position: center;
				background-size: 1300px 800px;
            } 
			header {
                color:white;
                text-align:center;
                padding:5px;
            }
            nav {
                color: azure;
                line-height:30px;
                height:300px;
                width:300px;
                float:left;
                padding:5px;	      
            }
            section {
                width:350px;
                float:left;
                padding:10px;	 	 
            }
            fieldset{
                width: 600px;
                float: inherit;
            }
            legend{
                font-size: 20px;
            }
            label.field{
                text-align: left;
                width: 200px;
                float: left;
                font-weight: bold;
            }
</style>
</head>
<body>
	<form action ="index" method ="GET">
	<h1 style="font-size: 50px; color: maroon;" align="center">Health Insurance Marketplace</h1>	
		<div class="nav">
			<p style="font-size: 25px; color: maroon;"><b>Business Trends and Patterns</b></p>
			<a href="https://public.tableau.com/profile/publish/PlanLevelAcrossDifferentStates/PlanLevelacrossStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Plan level across states</a><br>
			<a href="https://public.tableau.com/profile/publish/MarketCoveragelAcrossDifferentStates/MarketCoverageAcrossDifferentStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Market Coverage</a><br>
			<a href="https://public.tableau.com/profile/publish/MedianMonthlyPremiumDistribution/MedianMonthlyPremiumDistribution#!/publish-confirm" style="font-size: 20px; color: maroon;">Median Monthly Premium Distribution</a><br>
			<a href="https://public.tableau.com/profile/publish/Variationofrateswithnoofdependents/SubscriberRateAcrossStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Variation of rates with dependents</a>
		</div>
		<div align="center">
			<table>
				<tr>
					<td style="font-size: 25px; color: maroon;"><b>Select a state</b></td>
					<td><select id="state" name="state" style="color: maroon; font-size: 15px;">
							<option value="AZ">Arizona</option>
							<option value="AL">Alabama</option>
							<option value="AK">Alaska</option>
							<option value="FL">Florida</option>
							<option value="GA">Georgia</option>
							<option value="IN">Indiana</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 25px; color: maroon;"><b>Is your plan existing</b></td>
					<td><select id="isPlanNew" name="isPlanNew" style="color: maroon; font-size: 15px;">
							<option value="Existing">Yes</option>
							<option value="New">No</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 25px; color: maroon;"><b>Coverage Type</b></td>
					<td><select id="coverage" name="coverage" style="color: maroon; font-size: 15px;">
							<option value="Individual">Individual</option>
							<option value="SHOP(Small Group)">Group</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 25px; color: maroon;"><b>Offering for</b></td>
					<td><select id="childOnlyOffering" name="childOnlyOffering" style="color: maroon; font-size: 15px;">
							<option value="Allows Adult and Child-Only">Allows Adult and Child-Only</option>
							<option value="Allows Adult-Only">Allows Adult-Only</option>
							<option value="Allows Child-Only">Allows Child-Only</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 25px; color: maroon;"><b>Out of Country Coverage</b></td>
					<td><select id="outOfCountryCoverage" name="outOfCountryCoverage" style="color: maroon; font-size: 15px;">
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 20px; color: maroon;"><b>HSA OR HRA Amount (US Dollars)</b></td>
					<td><input type="text" id="hsaOrHra" name="hsaOrHra" style="font-size: 15px;"/></td>
				</tr>
				<tr><td>(If not eligible for HSA or HRA, please type NA)</td></tr>
			</table>
			<br>
			<br>
			<input type="button" value="Search for plans" onclick="suggestPlan()" style="font-size: 15px; color: maroon;"/> <br><br>
		</div>
		<div align="center">
			<table>
				<tr>
					<td style="font-size: 20px; color: maroon;"><b>Suggested Plan</b></td>
					<td><input type="text" id="planName" name="planName" readonly="readonly"/></td>
					<td style="font-size: 20px; color: maroon;"><b>Accuracy</b></td>
					<td><input type="text" id="accuracy" name="accuracy" readonly="readonly"/></td>
				</tr>
			</table>
			<br>
			<br>
		    <input type="submit" value="Proceed to check for rates" style="font-size: 15px; color: maroon;"/> <br><br>
			
		</div>
	</form> 
	<script>
	function suggestPlan(){
		var state = $("#state").val();
		var isPlanNew = $("#isPlanNew").val();
		var coverage = $("#coverage").val();
		var childOnlyOffering = $("#childOnlyOffering").val();
		var outOfCountryCoverage = $("#outOfCountryCoverage").val();
		var hsaOrHra = $("#hsaOrHra").val();
		$.ajax({
			type : "GET",
			url : "home",
			data : {
				state : state,
				isPlanNew : isPlanNew,
				coverage : coverage,
				childOnlyOffering : childOnlyOffering,
				outOfCountryCoverage : outOfCountryCoverage,
				hsaOrHra : hsaOrHra
			},
			dataType : "html",
			success : function(data) {
				var json = JSON.parse(data);
				console.log(json);
				
				
				var data1=json.Results.output1.value.Values[0][9];
				$("#planName").val(data1);
				var data2=json.Results.output1.value.Values[0][10];
				$("#accuracy").val(data2);
			}

		});
		
	}
	</script>
</body>
</html>