<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Health Insurance Marketplace</title>
<!-- Latest compiled and minified CSS 
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>
			body {
                background-image: url(http://previews.123rf.com/images/wawritto/wawritto1303/wawritto130300065/18539594-Abstract-molecules-medical-background-Vector--Stock-Vector.jpg);
				background-repeat: no-repeat;
				background-position: top;
				background-size: 1300px 800px;
            }
			header {
                color:white;
                text-align:center;
                padding:5px;
            }
            nav {
                color: azure;
                line-height:30px;
                height:300px;
                width:300px;
                float:left;
                padding:5px;	      
            }
            section {
                width:350px;
                float:left;
                padding:10px;	 	 
            }
            fieldset{
                width: 600px;
                float: inherit;
            }
            legend{
                font-size: 20px;
            }
            label.field{
                text-align: left;
                width: 200px;
                float: left;
                font-weight: bold;
            }
</style>
</head>
<body>	
	<form action ="home" method ="GET">	
		<h1 style="font-size: 50px; color: maroon;" align="center">Get a free health insurance quote</h1>
		
		<div class="nav" >
			<p style="font-size: 25px; color: maroon;"><b>Business Trends and Patterns</b></p>
			<a href="https://public.tableau.com/profile/publish/PlanLevelAcrossDifferentStates/PlanLevelacrossStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Plan level across states</a><br>
			<a href="https://public.tableau.com/profile/publish/MarketCoveragelAcrossDifferentStates/MarketCoverageAcrossDifferentStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Market Coverage</a><br>
			<a href="https://public.tableau.com/profile/publish/MedianMonthlyPremiumDistribution/MedianMonthlyPremiumDistribution#!/publish-confirm" style="font-size: 20px; color: maroon;">Median Monthly Premium Distribution</a><br>
			<a href="https://public.tableau.com/profile/publish/Variationofrateswithnoofdependents/SubscriberRateAcrossStates#!/publish-confirm" style="font-size: 20px; color: maroon;">Variation of rates with dependents</a><br><br>
		</div>
		<div>
			<a href="indexabc.jsp" style="font-size: 20px; color: maroon;">Go to Home Page</a>
		</div>
		<div align="center">
		<table>
			<tr>
				<td style="font-size: 25px; color: maroon;"><b>Select a plan type</b></td>
				<td><select id="planType" style="font-size: 15px; color: maroon;">
                    	<option value="EPO">EPO</option>
                    	<option value="PPO">PPO</option>
                    	<option value="POS">POS</option>
						<option value="HMO">HMO</option>
						<option value="Indemnity">Indemnity</option>
                </select></td>
			</tr>
			<tr>
				<td style="font-size: 25px; color: maroon;"><b>Enter your age</b></td>
				<td><input type="text" id="age" name="age" required style="font-size: 15px; color: maroon;"/></td>
			</tr>
			<tr>
				<td style="font-size: 25px; color: maroon;"><b>Tobacco Preference</b></td>
				<td><select id="tobacco" style="font-size: 15px; color: maroon;">
                    	<option value="Yes">Yes</option>
                    	<option value="No">No</option>
                    </select></td>
			</tr>
		</table>
		<br><br>
		<input type="button" id="predict" value="Predict Plan Rate" onclick="getRate()" style="font-size: 15px; color: maroon;"/>
		<br><br>
		<input type="text" id="planRate" name="planRate" style="font-size: 20px; color: maroon;"/>
         </div>
	</form> 
	<script>
	function getRate(){
		var planType = $("#planType").val();
		var age = $("#age").val();
		var tobacco = $("#tobacco").val();
		$.ajax({
			type : "POST",
			url : "index",
			data : {
				planType : planType,
				age : age,
				tobacco : tobacco
			},
			dataType : "html",
			success : function(data) {
				var json = JSON.parse(data);
				console.log(json);
				var data1=json.Results.output1.value.Values[0][6];
				$("#planRate").val(data1);

			}

		});
		
	}
	</script>
</body>
</html>