package com.me.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonObjectFormatVisitor;

@Controller
public class HomeController {
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(HttpServletRequest request,HttpServletResponse response, HttpSession session) throws IOException {

		PrintWriter out = response.getWriter();
		
		String a1 = request.getParameter("state");
		session.setAttribute("state", a1);
		String a2 = request.getParameter("isPlanNew");
		String a3 = request.getParameter("coverage");
		String a4 = request.getParameter("childOnlyOffering");
		String a5 = request.getParameter("outOfCountryCoverage");
		String a6 = request.getParameter("hsaOrHra");
		String planMarketingName = "";
		String metalLevel = "";
		
		if(a1.equalsIgnoreCase("AK")){
			planMarketingName = "Premier";
			metalLevel = "Low";
		}else if(a1.equalsIgnoreCase("AZ")){
			planMarketingName = "Humana Connect Bronze 6300/6300 Plan";
			metalLevel = "Bronze";
		}else if(a1.equalsIgnoreCase("AL")){
			planMarketingName = "UnitedHealthcare Gold HSA (UG-9 with A8)";
			metalLevel = "Gold";
		}else if(a1.equalsIgnoreCase("FL")){
			planMarketingName = "BlueOptions Predictable Cost 1413";
			metalLevel = "Gold";
		}else if(a1.equalsIgnoreCase("GA")){
			planMarketingName = "Ambetter Gold 2 + Vision";
			metalLevel = "Gold";
		}else if(a1.equalsIgnoreCase("IN")){
			planMarketingName = "Pediatric Plan 1";
			metalLevel = "Low";
		}
		
		
		
		//you may want to visit this website to see the structure of the data being returned by this URL
		String urlOfTheRestService = "https://ussouthcentral.services.azureml.net/workspaces/021f434c5e0847b18e21ab8381e88231/services/1791cba08b8a434da66dd66e08c7fa81/execute?api-version=2.0&details=true";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer RtDHRI4Rm4zHn5xPJyrnSJVdddgoIWwrREfh6hP3J6PLYTkFOKkRYGP+cys5IBMFlzvutdmZ+2JqVhtvQwxcvQ==");
		headers.add("Content-Length", "100000");
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		String requestJson = "{'Inputs': {'input1': {'ColumnNames': ['StateCode','MarketCoverage','PlanMarketingName','IsNewPlan','PlanType','MetalLevel','HSAOrHRAEmployerContributionAmount','ChildOnlyOffering','OutOfCountryCoverage'],'Values': [['"+a1+"','"+a3+"','"+planMarketingName+"','"+a2+"','null','"+metalLevel+"','"+a6+"','"+a4+"','"+a5+"']]}},'GlobalParameters': {} }";
		HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);

		//It simplifies communication with HTTP servers, and enforces RESTful principles
		RestTemplate restTemplate = new RestTemplate();

		
		try{
			String restData = restTemplate.postForObject(urlOfTheRestService, entity, String.class);
			
			JSONObject obj  = new JSONObject(restData);
			
			out.print(obj);
			request.setAttribute("quote", restData);
			
						
		}catch(HttpClientErrorException ex){
			System.out.println("Exception: "+ex.getResponseBodyAsString());
			System.out.println("Exception: "+ex.getMessage());
		}
		return null;
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String doIntitalizeForm(HttpServletRequest request){
		return "home";
	}
	
	@RequestMapping(value="/index", method=RequestMethod.POST)
	public String gethome(HttpServletRequest request,HttpServletResponse response, HttpSession session) throws IOException {

		PrintWriter out = response.getWriter();
		
		String a1 = request.getParameter("planType");
		String a2 = request.getParameter("age");
		String a3 = request.getParameter("tobacco");
		//String a4 = request.getParameter("state");
		String a4 = (String) session.getAttribute("state");
		String tobacco_no_prefer = "";
		String tobacco_users = "";
		
		
		if(a3.equalsIgnoreCase("Yes")){
			tobacco_no_prefer = "1";
			tobacco_users = "0";
		}else if(a3.equalsIgnoreCase("No")){
			tobacco_no_prefer = "0";
			tobacco_users = "1";
		}
		
		
		//you may want to visit this website to see the structure of the data being returned by this URL
		String urlOfTheRestService = "https://ussouthcentral.services.azureml.net/workspaces/021f434c5e0847b18e21ab8381e88231/services/a0d02f66977648fd9832790cc9109d03/execute?api-version=2.0&details=true";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer kxu78KtVIpENMkQ0FCXQftSl9IhtfjBVPmAgkv6SqZAKzWCrU/4O8ytZJZ5h2jWLD0gaBODJ1B54bw4ENrhJKw==");
		headers.add("Content-Length", "100000");
		headers.setContentType(MediaType.APPLICATION_JSON);
		String requestJson = "{'Inputs': {'input1': {'ColumnNames': ['StateCode','Age','IndividualRate','Plan Type','Tobacco_No Preference','Tobacco_Tobacco Users'],'Values': [['"+a4+"','"+a2+"','0','"+a1+"','"+tobacco_no_prefer+"','"+tobacco_users+"']]}},'GlobalParameters': {} }";
		HttpEntity<String> entity = new HttpEntity<String>(requestJson,headers);

		//It simplifies communication with HTTP servers, and enforces RESTful principles
		RestTemplate restTemplate = new RestTemplate();

		
		try{
			String restData = restTemplate.postForObject(urlOfTheRestService, entity, String.class);
			
			JSONObject obj  = new JSONObject(restData);
			out.print(obj);

			request.setAttribute("quote", restData);
			
		}catch(HttpClientErrorException ex){
			System.out.println("Exception: "+ex.getResponseBodyAsString());
			System.out.println("Exception: "+ex.getMessage());
		}
				return null;
	}


}
